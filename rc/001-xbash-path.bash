#$# manage the PATH variable

__xbash_path() {
    local segment

    for segment in $(ls "$XBASH/path"); do
        source "$XBASH/path/$segment"
        export PATH="$__path:$PATH"
        xinfo "        PATH=$__path:\$PATH"
    done
}

__xbash_path
unset __xbash_path __path
