#$# print this help message

_Dotfiles_Help() {
    local _base="$DOTFILES_HOME/lib/dotfiles"
    local _cmd_files="$(ls $_base)"

    _Cmd_Desc() {
        local _cmd_file="$1"
        grep -e '^\s*#$#' "$_base/$_cmd_file" | sed 's/^\s*#$#//'
    }

    echo "usage: dotfiles <command> [args...]"
    echo
    echo "dotfiles helps to maintain a sane bash shell environment."
    echo
    echo "Commands:"

    for _cmd_file in $_cmd_files; do
        if [ -f "$_base/$_cmd_file" ]; then
            local _cmd="$(echo $_cmd_file | sed 's/\.bash$//')"
            printf "\t%-16s%s\n" "$_cmd" "$(_Cmd_Desc $_cmd_file)"
        fi
    done
}

_Dotfiles_Help
