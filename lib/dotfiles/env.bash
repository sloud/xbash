#$# manage the users environment`

_Dotfiles_Env() {
    local _cmd="$1"
    shift

    _Delegate() {
        echo "${DOTFILES_HOME}/lib/dotfiles/env/${1}.bash"
    }

    if [ -f "$(_Delegate $_cmd)" ]; then
        bash "$(_Delegate $_cmd)" $@
    else
        echo "Unknown command [$_cmd]"
        bash "$(_Delegate help)"
        exit 1
    fi
}

_Dotfiles_Env $@
