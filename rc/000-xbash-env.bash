#$# environment variable setup

__xbash_env() {
    local var val

    for var in $(ls "$XBASH/env"); do
        val="$(cat "$XBASH/env/$var")"
        xinfo "        $var=$val"
        export "$var"="$val"
    done
}

__xbash_env
unset __xbash_env
