#$# list the variables in the environment

_Dotfiles_Env_Add() {
    if [ "$#" -eq 0 ]; then
        ls "$DOTFILES_HOME/env"
    else
        echo "usage: dotfiles env ls"
    fi
}

_Dotfiles_Env_Add $@
