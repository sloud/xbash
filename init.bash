
# If not running interactively, don't do anything
[[ $- != *i* ]] && return

[ -n "$XBASH" ] && source "$XBASH/lib/utils.bash" || return

__xbashrc() {
    # source bachrc fragments
    local frag xbashrc

    xinfo "---"
    xinfo "xbashrc fragments:"
    for frag in $(ls "$XBASH/rc"); do
        xbashrc="$XBASH/rc/$frag"
        xdesc $xbashrc
        
        source "$xbashrc"
    done
    xinfo "---"
}

__xbashrc
unset __xbashrc ${XBASH_UTILS[@]} XBASH_UTILS
