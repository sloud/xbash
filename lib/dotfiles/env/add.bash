#$# add a variable to the environment

_Dotfiles_Env_Add() {
    local _env_key="$1"
    local _env_val="$2"

    if [ "$#" -eq 2 ]; then
        echo "$_env_val" >"$DOTFILES_HOME/env/$_env_key"
        echo "$_env_key=$_env_val"
    else
        echo "usage: dotfiles env add <MY_VAR> <its-value>"
    fi
}

_Dotfiles_Env_Add $@
