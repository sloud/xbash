#$# print this help message

_Dotfiles_Env_Help() {
    local _base="$DOTFILES_HOME/lib/dotfiles/env"
    local _cmd_files="$(ls $_base)"

    _Cmd_Desc() {
        local _cmd_file="$1"
        grep -e '^\s*#$#' "$_base/$_cmd_file" | sed 's/^\s*#$#//'
    }

    echo "usage: dotfiles env <command> [args...]"
    echo
    echo "Commands:"

    for _cmd_file in $_cmd_files; do
        if [ -f "$_base/$_cmd_file" ]; then
            local _cmd="$(echo $_cmd_file | sed 's/\.bash$//')"
            printf "\t%-16s%s\n" "$_cmd" "$(_Cmd_Desc $_cmd_file)"
        fi
    done
}

_Dotfiles_Env_Help
