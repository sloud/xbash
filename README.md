# xbash
plugin system for bash

## install
copy over the files

    rsync -av . $HOME/.bashrc

put this in your `.bashrc`

    #export XBASH_VERBOSE=1
    export XBASH="$HOME/.xbash"
    [[ -s "$XBASH/init.bash" ]] && source "$XBASH/init.bash"
