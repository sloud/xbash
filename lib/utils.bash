XBASH_UTILS=()

XBASH_UTILS+=("xinfo")
xinfo() {
    if [ -n "$XBASH_VERBOSE" ]; then
        echo "$@"
    fi
}

XBASH_UTILS+=("xdesc")
xdesc() {
    local frag="$1"
    local frag_name="$(basename $frag)"
    local desc="$(grep -e '^\s*#$#' "$frag" | sed 's/^\s*#$#\s*//')"

    if [ -n "$desc" ]; then
        xinfo "    $frag_name - $desc"
    else
        xinfo "    $frag_name"
    fi
}
