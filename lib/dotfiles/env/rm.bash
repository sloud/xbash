#$# rm a variable from the environment

_Dotfiles_Env_Rm() {
    local _env_key="$1"

    if [ "$#" -eq 1 ]; then
        rm "$DOTFILES_HOME/env/$_env_key"
        echo "rm $_env_key"
    else
        echo "usage: dotfiles env rm <MY_VAR>"
    fi
}

_Dotfiles_Env_Rm $@
